import Vue from 'vue'
import App from '../pages/Index.vue'

new Vue({
  el: '#app',
  render: h => h(App)
})